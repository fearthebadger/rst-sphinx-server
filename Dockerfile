FROM python:3.8.5-alpine3.12

MAINTAINER Loïc Pauletto <loic.pauletto@gmail.com>
MAINTAINER Quentin de Longraye <quentin@dldl.fr>
MAINTAINER Brock Renninger <brock@fearthebadger.com>

COPY ./requirements.txt requirements.txt

ENV PYTHONPATH /usr/lib/python3.8/site-packages

RUN apk add --no-cache --virtual --update py3-pip py3-numpy grep make wget ca-certificates ttf-dejavu openjdk8-jre graphviz \
    && pip install --upgrade pip \
    && pip install --no-cache-dir  -r requirements.txt

RUN wget http://downloads.sourceforge.net/project/plantuml/plantuml.jar -P /opt/ \
    && echo -e '#!/bin/sh -e\njava -jar /opt/plantuml.jar "$@"' > /usr/local/bin/plantuml \
    && chmod +x /usr/local/bin/plantuml

RUN wget https://unpkg.com/vis-network@8.2.1/standalone/umd/vis-network.min.js -P /opt/sphinx-server/

COPY ./scripts/server.py /opt/sphinx-server/
COPY ./scripts/make_map.py /opt/sphinx-server/
COPY ./.sphinx-server.yml /opt/sphinx-server/

COPY ./scripts/notes_map.html /opt/sphinx-server/

WORKDIR /web

EXPOSE 8000

CMD ["python", "/opt/sphinx-server/server.py"]
