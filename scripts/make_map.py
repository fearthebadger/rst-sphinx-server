#!/usr/bin/env python3

import os
from os import path
import subprocess
import re
import numpy as np

def find_children(folder, note_links, nodes):

    for f in os.scandir(folder):

        if f.is_dir() and f.name not in [".git", "_build"]:

            child_folder = folder + f.name + "/"

            if path.exists(child_folder + "/index.rst"):

                nodes.append(child_folder)
                note_links.append({folder, child_folder})
                note_links, nodes = find_children( child_folder, note_links, nodes )

    return note_links, nodes

def find_doc_links(note_links):

    stdout = subprocess.check_output(
            "/bin/grep -Ro --include=*.rst -e \\:ref\\:\\`[^\\\"]*\\` /web/",
        universal_newlines=True,
        shell=True
    )

    link_start = stdout.splitlines()

    for link in link_start:

        link_part = link.split(':')

        link_word = re.sub('_$', '', link_part[1])
        link_start_dir = "./" + path.dirname(link_part[0]) + "/"

        # The exit 0 is a stupid work around to grep returning a 1 if nothing is
        # found.
        stdout = subprocess.check_output(
            "/bin/grep -Rl --include=*.rst -e ..\\\\s_" + link_word + ":$ /web/; exit 0",
            universal_newlines=True,
            shell=True
        )

        if stdout:
            # You should never have more than one link originator. So I'm going
            # to ignore anything but the first one.
            link_end = stdout.splitlines()[0]
            link_end_dir = "./" + path.dirname(link_end) + "/"

            note_links.append({link_start_dir, link_end_dir})

    return note_links


def write_js_script(note_links, nodes, build_folder):

    fp = open(build_folder + "/notes_map.js", "w")

    fp.write("var nodes = new vis.DataSet([\n")

    for index, node in enumerate(nodes):

        stdout = subprocess.check_output(
            "/bin/grep '^.. desc:' /web/" + node + "index.rst",
            universal_newlines=True,
            shell=True
        )

        label = re.sub('^.. desc:\\s', '', stdout).rstrip()
        fp.write("  {id: " + str(index))
        fp.write(", label: '" + label + "'")
        fp.write(", shape: \"box\"")
        fp.write("},\n")

    fp.write("]);")

    fp.write("\n\n")

    fp.write("var edges = new vis.DataSet([\n")

    for link_start, link_end in note_links:

        fp.write("  {from: " + str(nodes.index(link_start)))
        fp.write(", to: " + str(nodes.index(link_end)) + "},\n")

    fp.write("]);")

    fp.write("\n\n")

    fp.write("var container = document.getElementById('mynotes');\n")

    fp.write("var data = {\n")
    fp.write("  nodes: nodes,\n")
    fp.write("  edges: edges,\n")
    fp.write("};")

    fp.write("\n\n")

    fp.write("var options = {\n")
    fp.write("  nodes:{\n")
    fp.write("    borderWidth: 1,\n")
    fp.write("    chosen: true,\n")
    fp.write("    color: {\n")
    fp.write("      border: '#AAAAAA',\n")
    fp.write("      background: '#EEEEEE',\n")
    fp.write("      highlight: {\n")
    fp.write("        border: '#2B7CE9',\n")
    fp.write("        background: '#D2E5FF'\n")
    fp.write("      },\n")
    fp.write("      hover: {\n")
    fp.write("        border: '#2B7CE9',\n")
    fp.write("        background: '#D2E5FF'\n")
    fp.write("      }\n")
    fp.write("    }\n")
    fp.write("  }\n")
    fp.write("}\n")

# □ Build groups based on the first children indexes of Notes.
# □ Build dotted lines when linking between groups.
# □ Anchor the notes index to the center. (Maybe... the dashes may distinguish
#   enough
# □ Make the nodes a link to the notes page.
# ✓ Change the shape of the bubbles to rounded rectangles.
#   ✓ With light gray background and colored border.
#
#    var options = {
#      groups: {
#        test: {
#          color:{
#            background:'red',
#            border: 'green'
#          },
#          borderWidth: 10
#        }
#      }
#    }

    fp.write("var network = new vis.Network(container, data, options);")

    fp.close()
