.. _rst-sphinx-server:

##############################
reStructuredText Sphinx Server
##############################

This repository is cloned from: `dldl/sphinx-serger <https://github.com/dldl/sphinx-server>`_

Sphinx-Server allows you to build *Sphinx documentation* using a Docker
image based on Alpine.

Functionalities:
  - **Sphinx documentation** served by a python server
  - UML support with **PlantUML**
  - ``dot`` support with **Graphviz**
  - **Autobuild** with sphinx-autobuild
  - HTTP **authentication**

Limitations:
  - This image is not bundled with LaTeX but you can generate *.tex* files and
    compile them outside of the container.

Build
=====

Clone this repository on your computer and build the image using the following
command:

.. code-block:: console

    $ docker build -t sphinx-server:latest .

Usage
=====

You may add a **.sphinx-server.yml** file at the root of your project
documentation if you want to use a custom configuration. You can see the default
**.sphinx-server.yml** in this repository that will be used if you don't add
yours.

Container creation
==================

Without autobuild (production mode)
-----------------------------------

If you want to enable HTTP authentication, just add a **.sphinx-server.yml** file
at the root of your project documentation and add a ``credentials`` section. You
also need to set ``autobuild`` to false.

Run the following command at the root of your documentation:

.. code-block:: console

  $ docker run \
    --rm \
    -itd \
    -v "$(pwd)":/web \
    -p 8000:8000 \
    --name sphinx-server \
    sphinx-server

With autobuild enabled
----------------------

Autobuild is enabled by default. You may add folders and files to the ``ignore`` list.

Run the following command at the root of your documentation:

.. code-block:: console

  $ docker run \
    --rm \
    -itd \
    -v "$(pwd)":/web \
    -u $(id -u):$(id -g) \
    -p 8000:8000 \
    --name sphinx-server \
    sphinx-server

The web server will be listening on port ``8000``.
All the files in the current directory will be mounted in the container.

Run Command
-----------

``sphinx_rc`` in ``scripts/`` is a file you can source from your .bashrc to give you an easy command
to start the ``sphinx-server``.


